package com.example.lorencoo.szablon.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Update;

import com.example.lorencoo.szablon.data.myData.MyData;

@Dao
public interface MyDao {

    @Insert
    long insert(MyData myData);

    @Update
    public int updateStudents(MyData myData);

    @Delete
    void delete(MyData myData);
}
