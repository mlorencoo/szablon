package com.example.lorencoo.szablon.data;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefsManager {
    private static final String FILE_NAME = "com.example.lorencoo.sdaapp.data.PREFS_FILE";

    private SharedPreferences sharedPreferences;

    public PrefsManager(Context context) {
        sharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

}
