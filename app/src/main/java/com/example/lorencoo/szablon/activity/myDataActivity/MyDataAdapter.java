package com.example.lorencoo.szablon.activity.myDataActivity;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lorencoo.szablon.R;
import com.example.lorencoo.szablon.data.myData.MyData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyDataAdapter extends RecyclerView.Adapter<MyDataAdapter.ViewHolder> {

    public static final String MYDATA_ID = "MYDATA_ID";
    private List<MyData> myDataList = new ArrayList<>();
    private int selectedItemPosition = -1;

    public void updateRockets(List<MyData> myDatas) {
        myDataList.clear();
        myDataList.addAll(myDatas);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_mydata, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupMyData(myDataList.get(position));

    }

    @Override
    public int getItemCount() {
        return myDataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_mydata_text)
        TextView competitionText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void setupMyData(MyData myData ) {
            competitionText.setText(myData.getId());
            competitionText.setOnClickListener(view -> {
                if (getAdapterPosition() == selectedItemPosition) {
                    selectedItemPosition = -1;
                    itemView.setBackgroundColor(Color.GREEN);
                }
//                Intent intent = new Intent(itemView.getContext(), CompetitionActivity.class);
//                intent.putExtra(MYDATA_ID, myData.getId());
//                itemView.getContext().startActivity(intent);
            });
        }

    }
}
