package com.example.lorencoo.szablon.di;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.lorencoo.szablon.ApplicationScope;
import com.example.lorencoo.szablon.api.Api;
import com.example.lorencoo.szablon.dao.MyDao;
import com.example.lorencoo.szablon.data.PrefsManager;
import com.example.lorencoo.szablon.database.AppDatabase;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataModule {

    @Provides
    @ApplicationScope
    PrefsManager providesPrefsManager(Context context) {
        return new PrefsManager(context);
    }

    @Provides
    @ApplicationScope
    AppDatabase provideAppDataBase(Context context) {
        return Room.databaseBuilder(context,
                AppDatabase.class, "database-name")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @ApplicationScope
    MyDao providesStudentDao(AppDatabase appDatabase) {
        return appDatabase.myDao();
    }

    @Provides
    @ApplicationScope
    Retrofit providesRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

}
