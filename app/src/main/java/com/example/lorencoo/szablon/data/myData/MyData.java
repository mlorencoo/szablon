package com.example.lorencoo.szablon.data.myData;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class MyData {

    @PrimaryKey
    private int id;

    //    @SerializedName("last_updated")
//    private NextData nextData;
//    @SerializedName("next_data")
//    @Embedded
//
//    private String lastUpdated;
//    @ColumnInfo(name = "last_updated")
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

}
