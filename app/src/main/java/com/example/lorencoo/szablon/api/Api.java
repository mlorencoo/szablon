package com.example.lorencoo.szablon.api;

import com.example.lorencoo.szablon.data.myData.MyData;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface Api {

    String BASE_URL = "";

    @GET("")
    Single<List<MyData>> getMyData();

}
