package com.example.lorencoo.szablon.activity.myDataActivity;

import com.example.lorencoo.szablon.data.myData.MyData;

import java.util.List;

public interface MyDataContract {
    interface View {
        void showError();

        void showData(List<MyData> competitions);

        void showProgress();
    }

    interface Presenter {
        void onTryAgainClick();

        void doYourUpdate();
    }
}
