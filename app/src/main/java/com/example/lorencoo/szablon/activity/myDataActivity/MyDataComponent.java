package com.example.lorencoo.szablon.activity.myDataActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {MyDataModule.class})
public interface MyDataComponent {
    void inject(MyDataActivity myDataActivity );
}
