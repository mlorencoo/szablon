package com.example.lorencoo.szablon.activity.myDataActivity;

import android.os.Bundle;
import android.support.constraint.Group;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;


import com.example.lorencoo.szablon.MyApplication;
import com.example.lorencoo.szablon.R;
import com.example.lorencoo.szablon.data.myData.MyData;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyDataActivity extends AppCompatActivity implements MyDataContract.View {

    @BindView(R.id.swipe_refresh_myData)
    SwipeRefreshLayout swipeRefreshmyData;

    @BindView(R.id.recycler_myData)
    RecyclerView myDataRycacler;

    @BindView(R.id.progres_bar_myData)
    ProgressBar myDataProgress;

    @BindView(R.id.myData_error_group)
    Group myDataErrorGroup;

    private MyDataAdapter myDataAdapter;

    @Inject
    MyDataContract.Presenter presenter;


    @OnClick(R.id.myData_button)
    public void onTryAgainClick() {
        presenter.onTryAgainClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_data);
        ButterKnife.bind(this);

        ((MyApplication)getApplication()).getAppComponent()
                .plus(new MyDataModule(this))
                .inject(this);

        myDataAdapter = new MyDataAdapter();

        myDataRycacler.setLayoutManager(new LinearLayoutManager(this));
        myDataRycacler.setAdapter(myDataAdapter);

        swipeRefreshmyData.setOnRefreshListener(() -> presenter.doYourUpdate());
    }

    @Override
    public void showError() {
        myDataProgress.setVisibility(View.INVISIBLE);
        myDataErrorGroup.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<MyData>  myDataList) {
        swipeRefreshmyData.setRefreshing(false);
        myDataProgress.setVisibility(View.INVISIBLE);
        myDataRycacler.setVisibility(View.VISIBLE);
        myDataAdapter.updateRockets(myDataList);
    }

    @Override
    public void showProgress() {
        myDataRycacler.setVisibility(View.INVISIBLE);
        myDataErrorGroup.setVisibility(View.INVISIBLE);
        myDataProgress.setVisibility(View.VISIBLE);
    }
}
