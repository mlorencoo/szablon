package com.example.lorencoo.szablon;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;
import android.util.Log;


import com.example.lorencoo.szablon.database.AppDatabase;
import com.example.lorencoo.szablon.di.AppComponent;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

import static timber.log.Timber.DebugTree;

public class MyApplication extends Application {
    private  AppDatabase db;

    private AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new DebugTree() {
                @Override
                protected String createStackElementTag(StackTraceElement element) {
                    return super.createStackElementTag(element) + " *** " + element.getLineNumber();
                }
            });

        } else {
            Timber.plant(new CrashReportingTree());
        }
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "database-name")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();

        Realm.init(this);
        RealmConfiguration realmConfiguration =new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        //dagger

    }

    public  AppDatabase getDb() {
        return db;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }


    /**
     * A tree which logs important information for crash reporting.
     */
    private static class CrashReportingTree extends Timber.Tree {
        @Override
        protected void log(int priority, String tag, @NonNull String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }

        }
    }
}