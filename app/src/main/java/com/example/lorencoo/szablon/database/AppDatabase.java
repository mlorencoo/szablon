package com.example.lorencoo.szablon.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.lorencoo.szablon.dao.MyDao;
import com.example.lorencoo.szablon.data.myData.MyData;


@Database(entities = {MyData.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MyDao myDao();
}