package com.example.lorencoo.szablon.activity.myDataActivity;

import com.example.lorencoo.szablon.api.Api;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class MyDataModule {
    private MyDataContract.View view;

    public MyDataModule(MyDataContract.View view) {
        this.view = view;

    }
    @Provides
    @Singleton
    MyDataContract.Presenter provideCometitionPresenter(Retrofit retrofit){
        return new MyDataPresenter(view,retrofit.create(Api.class));
    }
}
