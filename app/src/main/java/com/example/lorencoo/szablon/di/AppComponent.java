package com.example.lorencoo.szablon.di;

import com.example.lorencoo.szablon.ApplicationScope;
import com.example.lorencoo.szablon.activity.myDataActivity.MyDataComponent;
import com.example.lorencoo.szablon.activity.myDataActivity.MyDataModule;

import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class, DataModule.class})
public interface AppComponent {
        MyDataComponent plus(MyDataModule myDataModule);
}
