package com.example.lorencoo.szablon.activity.myDataActivity;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.example.lorencoo.szablon.api.Api;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MyDataPresenter implements MyDataContract.Presenter, LifecycleObserver {
    private MyDataContract.View view;
    private Api api;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public MyDataPresenter(MyDataContract.View view, Api api) {
        this.view = view;
        this.api = api;
        ((LifecycleOwner) this.view).getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onStart() {
        getMyDatas();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onDestroy() {
        compositeDisposable.clear();
    }


    @Override
    public void onTryAgainClick() {
        getMyDatas();
    }

    @Override
    public void doYourUpdate() {
        getMyDatas();
    }

    private void getMyDatas() {
        view.showProgress();
        compositeDisposable.add(api.getMyData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        list ->
                                view.showData(list),
                        throwable -> view.showError()
                )
        );
    }
}
